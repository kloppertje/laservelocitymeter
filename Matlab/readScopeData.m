
comPort = 'COM9';

%% Open objects
s = serial(comPort, 'BaudRate', 115200);


%%
h = DS1054Z('192.168.1.3');



%%
% Decrease memory depth to reduce transfer time
h.Run;
% h.MDEPTH = 300000;
h.MDEPTH = 600000;
h.Stop;

% triggerChannelAnA = 1;
% triggerChannelAnB = 2;
% triggerChannelA = 3;
% triggerChannelB = 4;
triggerChannelA = 1;
triggerChannelB = 3;

h.T_SCALE = 0.05;
h.TRIG_EDGE_CHN = sprintf('CHAN%d', triggerChannelA);
h.TRIG_EDGE_SLOPE = 'NEG';
h.TRIG_SWEEP = 'SING';
h.TRIG_EDGE_LEVEL = 1.5; % Roughly half of 3.3V

h.LeftTrigger;
% Measure analog output voltage
% h.T_SCALE = 0.001;
% h.Run;
% pause(1) % Wait for signal to complete
% vAn = str2double(deblank(query(h.com, sprintf(':MEASure:ITEM? VAVG,CHANnel%d',triggerChannelA))));
% vTrig = vAn*0.95;
% h.Stop

% Set trigger slightly lower
% h.TRIG_EDGE_LEVEL = vTrig;



%% Gather array of observations
tA = [];
vA = [];
%%

% Arm scope
h.Single;

fopen(s);

while 1
    if s.BytesAvailable>0
        l = deblank(fgetl(s));
        values = sscanf(l, '%f,%f');
        vSer = values(1);
        tSer = values(2)/72e6;
        break;
    end
    pause(0.01)
end

fclose(s)
pause(0.5)
% Serial port has given a value, so now scope must contain data
[x,fs,t] = h.WaveAcquire([1 3],0);

% figure(1)
% plot(t,x)

% Find trigger point(s)
% vAn = x(:,1);
vDig = x(:,1:2);

indTrigA = find(vDig(:,1)<1.5,1);
tTrigA = t(indTrigA);

indTrigB = find(vDig(:,2)<1.5,1);
tTrigB = t(indTrigB);

tScope = tTrigB-tTrigA;
vScope = 0.1/tScope;

tError = (tScope-tSer)/tScope;
vError = (vScope-vSer)/vScope;

fprintf('Serial: v = %6.4f m/s, t = %6.4f ms\n', vSer,tSer*1e3)
fprintf('Scope:  v = %6.4f m/s, t = %6.4f ms\n', vScope,tScope*1e3)
fprintf('Error:  v %6.2f %%  , t %6.2f %%\n', vError*100,tError*100)

% There seems to be a "fixed" error in time, as relative error grows with
% higher velocity. Maybe plot velocity / time vs. error?

tA = [tA; [tSer tScope]];
vA = [vA; [vSer vScope]];

%%
figure(2)
plot(tA(:,2),tA(:,1), 'o')
xlabel('Scope')
ylabel('Serial')
title('Time between triggers from serial and scope [s]')
grid on

% Perform linear regression
A = tA(:,1);
B = [ones(size(A)) tA(:,2)];
c = B\A;

hold on
xl = get(gca, 'XLim');
plot(xl, c(1)+xl*c(2), 'k--')
hold off
% print('img/timeRegression', '-dpng')

%% Plot error in velocity
figure(3)
plot(vA(:,2),(tA(:,1)-tA(:,2))*1000,'o')
grid on
ylabel('Time difference, scope - meter [ms]')
xlabel('Measured velocity [m/s]')
ylim([ 0 1])
% print('img/timeError', '-dpng')

%% Afterwards, restore memory depth 
h.MDEPTH = 'AUTO';