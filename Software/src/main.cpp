#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Streaming.h>
// #include <Adafruit_SSD1306_STM32.h>
#include <Adafruit_SH1106_STM32.h>
#include <Fonts/FreeSans18pt7b.h>

// #define PIN_TRIGGER_A PB11
// #define PIN_TRIGGER_B PB10
// #define PIN_TRIGGER_A_AN PB1
// #define PIN_TRIGGER_B_AN PB0
// #define PIN_LASER_A PA2
// #define PIN_LASER_B PA3
#define PIN_TRIGGER_A PB10
#define PIN_TRIGGER_B PB11
#define PIN_TRIGGER_A_AN PB0
#define PIN_TRIGGER_B_AN PB1
#define PIN_LASER_A PA3
#define PIN_LASER_B PA2

#define PIN_BATT_VOLTAGE PA0

#define PIN_LED PB12

#define PIN_DEBUG PB13


#define BATT_VOLTAGE_MEAS_PERIOD_MS 50
#define BATT_VOLTAGE_UPDATE_PERIOD_MS 500

#define BRIGHTNESS_GOAL 2200 // ADC count target for photodiode, used for laser brightness calibration
#define PWM_MAX 1023
#define TICKS_PER_SECOND 72000000
#define TICKS_PER_OVERFLOW 72000
#define GAP_DISTANCE 0.1

#define OLED_RESET 4
Adafruit_SH1106 display(OLED_RESET);

// const uint8_t image_data_batterySymol5x8[5] = {0b1000001,0b01000010,0b00100100,0b00011000,0b10000001};
const uint8_t image_data_batterySymbol8x5[5] = {0b11111100,0b10000111,0b10000111,0b10000111,0b11111100};

HardwareTimer timer(1);
HardwareTimer pwmTimer(2);
// Better don't use timer 3, as that one is used for functions like delay(). You'll have a fun time debugging if you do use it (I did).

volatile uint16_t timerCounter = 0;
volatile uint8_t timerState = 0; // 0 = ready for start, 1 = trigger A, 2 = trigger B after trigger A, 3 = timeout?

volatile uint32_t timeBetweenTrigger = 0;

// The timer.resume() is calling timer_resume(), which is an inline function that sets a bit in a register. This might take a few cycles. 
// Also, according to the datasheet, the timer starts running 1 cycle after setting the enable bit (CEN).
// @72MHz, 1 clock cycle is ~13.9 ns. So, say we have a delay of 10 cycles = 139 ns. 
// For a speed of 100 m/s, the gates are passed in 1000 us. So, the error in velocity will be 0.0139%, which is doable for me.
void triggerAInt(void) {
  if (timerState==0) {
    timer.resume(); 
    timerState = 1; 
  }
}

volatile boolean debugPinState = 0;
void triggerBInt(void) {
  if (timerState==1) {
    timer.pause();
    timeBetweenTrigger = timerCounter*0xFFFF + timer.getCount();

    // Check for too fast reading (caused by EMI spikes)
    if (timeBetweenTrigger<100) {
      timerState = 0;
    } else {
      timerState = 2;
    }
    timer.setCount(0);
    timerCounter = 0;
    debugPinState = 0;
    digitalWrite(PIN_DEBUG, debugPinState);
  }
}

void timerOvfInt(void) {
  timerCounter++;
  digitalWrite(PIN_DEBUG, debugPinState);
  debugPinState = !debugPinState;
}

void checkLaserAlignment(void);
void calibrateLaser(void);


void setup()   {
  Serial.begin(115200);

  pinMode(PIN_TRIGGER_A, INPUT);
  pinMode(PIN_TRIGGER_B, INPUT);
  pinMode(PIN_LED, OUTPUT);

  pinMode(PIN_LASER_A, PWM);
  pinMode(PIN_LASER_B, PWM);

  pinMode(PIN_DEBUG, OUTPUT);
  digitalWrite(PIN_DEBUG, debugPinState);

  // Setup pwmTimer for a pwm frequency of 72Mhz/1024 = 70.3 kHz
  pwmTimer.setPrescaleFactor(1);
  pwmTimer.setOverflow(PWM_MAX+1);
  // pwmTimer.setCompare(TIMER_CH3,250); // To be clear (I was confused by it): TIMER_CH3 means the 3rd (0 1 2 3) output compare unit of pwmTimer (timer 2)
  pwmWrite(PIN_LASER_A, 0);
  pwmWrite(PIN_LASER_B, 0);
  delay(100);

  Serial.println("Boot");

  display.begin(SH1106_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();

  // text display tests
  display.setTextColor(WHITE, BLACK);
  display.setCursor(0,0);
  display.println("Velocity meter 0.1");
  display.display();
  delay(1000);

  Serial.println("Start");

  checkLaserAlignment();
  calibrateLaser();
  delay(500);

  display.clearDisplay();
  display.drawBitmap(90, 1, image_data_batterySymbol8x5, 8, 5, 1);
  display.setTextSize(1);
  display.display();

  attachInterrupt(PIN_TRIGGER_A, triggerAInt, FALLING);
  attachInterrupt(PIN_TRIGGER_B, triggerBInt, FALLING);

  // Timer setup
  timer.pause();
  timer.setPrescaleFactor(1);
  timer.setOverflow(0xFFFF);
  timer.setMode(TIMER_CH1, TIMER_OUTPUT_COMPARE);
  timer.setCompare(TIMER_CH1, 0x7FFF);  // Shouldn't really be necessary
  timer.attachInterrupt(0, timerOvfInt); // Neat hack: by using 0 instead of timer 1-4, the update event intterupt enable is selected, instead of timer1-4 compare interrupt
  TIMER1_BASE->CR1 |= TIMER_CR1_URS;
  timer.setCount(0);
  timer.refresh();
  timerCounter = 0;
}

/*
To do:
Make class / separate files?
*/

#define V_HISTORY_LENGTH 4
void loop() {
  static uint16_t avgBattVoltage = 0;
  static uint32_t tLast_batt_meas = 0;
  static uint32_t tLast_batt_update = 0;
  uint32_t tNow = millis();

  static float vHist[V_HISTORY_LENGTH] = {0};
  static uint8_t numMeasurements = 0;

  if (tNow-tLast_batt_meas>BATT_VOLTAGE_MEAS_PERIOD_MS) {
    if (avgBattVoltage==0) avgBattVoltage = analogRead(PIN_BATT_VOLTAGE);
    tLast_batt_meas = tNow;
    avgBattVoltage = (avgBattVoltage*15 + analogRead(PIN_BATT_VOLTAGE))/16;

    // display.setCursor(100,20);
    // display.print(digitalRead(PIN_TRIGGER_A));
    // display.print(" ");
    // display.println(analogRead(PIN_TRIGGER_A_AN));
    // display.print(digitalRead(PIN_TRIGGER_B));
    // display.print(" ");
    // display.println(analogRead(PIN_TRIGGER_B_AN));
    // display.print(timerState);
    // display.print(" ");
    // display.println(timeBetweenTrigger);
    // display.display();
  }


  // updateDisplay() function? Options: battery, velocity, PD voltage, all.

  if (tNow-tLast_batt_update>BATT_VOLTAGE_UPDATE_PERIOD_MS) {
    tLast_batt_update = tNow;
    // display.clearDisplay();
    display.setCursor(100,0);
    display.setTextSize(1);
    display.print(avgBattVoltage*6.6/4095);
    display.display();
  }

  // Timeout after 1 second
  if (timerState==1 && timerCounter>(TICKS_PER_SECOND/TICKS_PER_OVERFLOW)) {
    timer.pause();
    timerState = 0;
    timer.setCount(0);
    timerCounter = 0;
  }

  if (timerState==2) {
    numMeasurements++;

    // float dT = timeBetweenTrigger/((float) TICKS_PER_SECOND);
    // float v = 0.1/dT;
    float v = 0.1*((float) TICKS_PER_SECOND)/((float) timeBetweenTrigger);

    for (uint8_t i=V_HISTORY_LENGTH-1; i>0; i--) {
      vHist[i] = vHist[i-1];
    }
    vHist[0] = v;

    // Print current speed in large font
    float f = vHist[0];
    display.setCursor(0,32);
    display.setTextSize(3);
    // display.setFont(&FreeSans18pt7b);
    if (f<10) display.print(" ");
    if (f<100) display.print(" ");
    display.print(f);

    display.setTextSize(1);
    display.setCursor(110,40); // Old: 94
    display.print("m/s");
    display.setCursor(64,57);

    // Also print in km/h units
    f = f*3.6;
    if (f<10) display.print(" ");
    if (f<100) display.print(" ");
    if (f>1000) f=999.99; // Prevent display layout from going haywire
    display.print(f);
    display.setCursor(104,57);
    display.print("km/h");

    // Print history of previous speeds
    display.setCursor(0,0);
    for (uint8_t i=V_HISTORY_LENGTH-1; i>0; i--) {
      f = vHist[i];
      if (f<10) display.print(" ");
      if (f<100) display.print(" ");
      if (f>1000) f=999.99;
      display.println(f);
    }

    display.display();

    Serial.print(v,4);
    Serial.print(',');
    Serial.print(timeBetweenTrigger);
    Serial.print(',');
    float f2 = ((float) timeBetweenTrigger)/TICKS_PER_SECOND;
    Serial.println(f2,8);

    timerState = 0;
    delay(100); 
  }
}


void checkLaserAlignment(void) {
  display.setCursor(0,0);
  display.println("Checking alignment");
  pwmWrite(PIN_LASER_A, 0);
  pwmWrite(PIN_LASER_B, 0);
  delay(100);

  uint16_t valABase = analogRead(PIN_TRIGGER_A_AN);
  uint16_t valBBase = analogRead(PIN_TRIGGER_B_AN);

  pwmWrite(PIN_LASER_A, PWM_MAX);
  pwmWrite(PIN_LASER_B, PWM_MAX);
  delay(100);

  while (1) {
    uint16_t valA = analogRead(PIN_TRIGGER_A_AN);
    uint16_t valB = analogRead(PIN_TRIGGER_B_AN);

    display.setCursor(0, 10);
    display << "PD A: " << valA << endl;
    display << "PD B: " << valB << endl;
    display.display();
    delay(100);

    if (valA>((valABase*4)/3) && valB>((valBBase*4)/3)) break;
  }

  pwmWrite(PIN_LASER_A, 0);
  pwmWrite(PIN_LASER_B, 0);

  display.println("Done");
  display.display();

  return;
}

void calibrateLaser(void) {
  // Adust laser brightness
  static uint16_t pwmValueA = 0, pwmValueB = 0;

  display.println("Cal. laser current");
  display.display();

  while (1) {
    boolean doneA = 0, doneB = 0;
    pwmWrite(PIN_LASER_A, pwmValueA);
    pwmWrite(PIN_LASER_B, pwmValueB);
    delay(10);

    int16_t voltageA = analogRead(PIN_TRIGGER_A_AN);
    int16_t voltageB = analogRead(PIN_TRIGGER_B_AN);
    float errorA = (BRIGHTNESS_GOAL-voltageA);
    float errorB = (BRIGHTNESS_GOAL-voltageB);

    doneA = abs(errorA)<10;
    doneB = abs(errorB)<10;

    Serial << pwmValueA << "\t" << voltageA << "\t" << errorA << "\t" << doneA << "\t" << pwmValueB << "\t" << voltageB << "\t" << errorB << "\t" << doneB << endl;

    if (doneA && doneB) break;

    if (!doneA) pwmValueA += (int16_t) (errorA/10.0);
    if (!doneB) pwmValueB += (int16_t) (errorB/10.0);

    if (pwmValueA>PWM_MAX) pwmValueA = PWM_MAX;
    if (pwmValueB>PWM_MAX) pwmValueB = PWM_MAX;

    display.setCursor(0,42);
    display << "PWM A " << pwmValueA << ", PD " << voltageA << " " << endl;
    display << "PWM B " << pwmValueB << ", PD " << voltageB << " " << endl;
    display.display();

    if (Serial.available()) {
      Serial.read();
      pwmValueA = 0;
      pwmValueB = 0;
    }
  }

  display.println("Done");
  display.display();
  return;
}